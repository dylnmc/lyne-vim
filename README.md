LYNE-VIM
========
##### Yet another vim statusline
---------------------------------
Each lyne is composed of multiple sections on the left and the right; these
segments are each divided by a separator. Each section is compartmentalized
into smaller segments, each of which is divided by a divider.

A segment is merely a function that is run and returns a string. You can easily
create your own segment or use some of the built-in segments.

This plugin is still under active development and fairly unattractive currently,
but it's been fun creating it. :D

### Quick Setup
#### Default Options 

By default, powerline symbols are used. If you do not have the proper fonts
installed, or you do not like powerline fonts, you can easily change the default
options.

```vim
let g:lyne_options = {
    \'leftSeparator': '',
    \'rightSeparator': '',
    \'leftDivider': '',
    \'rightDivider': '',
    \'modifiedIndicator': '[+]',
    \'noSeparate': ['lyne#getModified'],
    \'modify_color_left_num': -1,
    \'modify_color_right_num': -1
\}

let g:lyne_segments = {
    \'left':  [ ['lyne#getMode'], ['lyne#getName', 'lyne#getModified'] ],
    \'right': [ [], ['lyne#getLineNumber'] ]
\}

let g:lyne_currentmode = {
    \ 'n'      : 'N',
    \ 'no'     : 'N·Operator Pending',
    \ 'v'      : 'V',
    \ 'V'      : 'V·Line',
    \ "\<c-v>" : 'V·Block',
    \ 's'      : 'Select',
    \ 'S'      : 'S·Line',
    \ "\<c-s>" : 'S·Block',
    \ 'i'      : 'I',
    \ 'R'      : 'R',
    \ 'Rv'     : 'V·Replace',
    \ 'c'      : 'Command',
    \ 'cv'     : 'Vim Ex',
    \ 'ce'     : 'Ex',
    \ 'r'      : 'Prompt',
    \ 'rm'     : 'More',
    \ 'r?'     : 'Confirm',
    \ '!'      : 'Shell',
    \ 't'      : 'Terminal'
\}
```

#### Default HighLight Groups

```vim
" Mode
autocmd VimEnter,ColorScheme * highlight LyneModeNormal  ctermfg=8 ctermbg=4 guifg=black guibg=#16b0f7
autocmd VimEnter,ColorScheme * highlight LyneModeInsert  ctermfg=8 ctermbg=3 guifg=black guibg=yellow
autocmd VimEnter,ColorScheme * highlight LyneModeVisual  ctermfg=8 ctermbg=5 guifg=black guibg=#ac54c9
autocmd VimEnter,ColorScheme * highlight LyneModeReplace ctermfg=8 ctermbg=1 guifg=black guibg=#f44216
" Left Defaults
autocmd VimEnter,ColorScheme * highlight LyneLeft1  ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
autocmd VimEnter,ColorScheme * highlight LyneLeft2  ctermfg=7 ctermbg=0 guifg=white guibg=#2c2c2c
autocmd VimEnter,ColorScheme * highlight LyneLeft3  ctermfg=8 ctermbg=6 guifg=black guibg=cyan
" Right Defaults
autocmd VimEnter,ColorScheme * highlight LyneRight1 ctermfg=8 ctermbg=4 guifg=black guibg=#16b0f7
autocmd VimEnter,ColorScheme * highlight LyneRight2 ctermfg=7 ctermbg=0 guifg=white guibg=#2c2c2c
" Modified 
autocmd VimEnter,ColorScheme * highlight LyneModified ctermfg=NONE ctermbg=5 guifg=NONE guibg=#ac54c9
```

#### Default Look (gvim)
![statusline colors statusline screenshot](assets/all_modes.png)

#### (Notes)
* Since `g:lyne_segments` stores *arrays*, and you must recreate all of the segments you wish to have.

### Customization
* create your own functions
* use the built-in lyne functions

#### Adding Segments
```vim
function! g:LyneGit(...)
    let l:active = a:1 == winnr()
    if (l:active && exists('*fugitive#statusline'))
        let l:branch = fugitive#head()
        return l:branch ==# '' ? '' : ' '.l:branch
    endif
    return ''
endfunction

let g:lyne_segments = {
    \'left':  [ ['lyne#getBufferNumber', 'lyne#getMode'], ['g:LyneGit', 'lyne#getName', 'lyne#getModified'] ],
    \'right': [ [], ['lyne#getLineNumber'] ]
\}

```

#### Setting HighLight Groups
* If you are the creator of a colorscheme and are looking to support lyne, you
  should override all of the above highlight groups with: 
  `autocmd VimEnter,ColorScheme * highlight {GROUP} {OPTIONS}`
    - It's also a good idea to put that inside an `augroup`
* You can also change a couple of the hl groups in your vimrc:
    - In your vimrc, you can just bind to autocmd `ColorScheme`

```vim
augroup LyneColors
    autocmd!
    autocmd ColorScheme * highlight LyneModeNormal ctermbg=7 ctermfg=8 guibg=white guifg=#2e2e2e
    autocmd ColorScheme * highlight LyneModified ctermbg=0 ctermfg=6 guibg=#2e2e2e guifg=cyan
augroup end
```

##### after adding the above segment and changing the above highlight groups

* This is my statusline in cterm using the colorscheme, [nord](https://github.com/arcticicestudio/nord-vim)

![vimrc colors statusline screenshot](assets/vimrc_normal.png)

## Options Meanings
* `leftSeparator`: Separates each section on the left side
* `rightSeparator`: Separates each section on the right side
* `leftDivider`:  Separates each segment on the left side
* `rightDivider`: Separates each segment on the right side
* `modifiedIndicator`: Text when buffer is modified
* `noSeparate`: List of segments that should not have a divider before them
* `modify_color_left_num`: which section on the left to color when buffer is modified
* `modify_color_right_num`: which section on the right to color when buffer is modified

## Tid Bits

* Help

![help screenshot](assets/help.png)

* Buffer Modified

![modified buffer screenshot](assets/mod_buffer.png)

---

TODO
====
* add more default segments
* consider moving default segments into a subdirectory (no access to s:vars then)
* cache colors in `lyne#reload()` so that `lyne#statusline()` is not doing so much work
* try to move all functions into the statusline so that `lyne#statusline()` isn't called every cursormove
    - might not be possible
* just keep improving it! :)
