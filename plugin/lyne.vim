scriptencoding utf-8
if exists('g:loaded_lyne')
    finish
endif
let g:loaded_lyne = 1

autocmd VimEnter,WinEnter,WinLeave,BufWinEnter,BufReadPost,FileReadPost * call lyne#reload()

autocmd! User GoyoEnter nested call <sid>GoyoReload()
autocmd! User GoyoLeave nested call <sid>GoyoReload()


if (synIDattr(synIDtrans(hlID('StatusLine')), 'reverse', 'cterm'))
    let s:fgbg = 'fg'
else
    let s:fgbg = 'bg'
endif
let s:ctermbgStl = synIDattr(synIDtrans(hlID('StatusLine')), s:fgbg, 'cterm')
let s:guibgStl = synIDattr(synIDtrans(hlID('StatusLine')), s:fgbg, 'gui')

function! s:GoyoReload()
    if (s:ctermbgStl != '')
        execute 'highlight StatusLine ctermbg='.s:ctermbgStl
    endif
    if (s:guibgStl != '')
        execute 'highlight StatusLine guibg='.s:guibgStl
    endif
    call lyne#reload()
endfunction

augroup LyneModeDefaults
    autocmd!
    " Mode Defaults
    autocmd VimEnter,ColorScheme * highlight LyneModeNormalDefault  ctermfg=8 ctermbg=4 guifg=black guibg=#16b0f7
    autocmd VimEnter,ColorScheme * highlight LyneModeInsertDefault  ctermfg=8 ctermbg=3 guifg=black guibg=yellow
    autocmd VimEnter,ColorScheme * highlight LyneModeVisualDefault  ctermfg=8 ctermbg=5 guifg=black guibg=#ac54c9
    autocmd VimEnter,ColorScheme * highlight LyneModeReplaceDefault ctermfg=8 ctermbg=1 guifg=black guibg=#f44216
    " Left Defaults
    autocmd VimEnter,ColorScheme * highlight LyneLeft1Default  ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
    autocmd VimEnter,ColorScheme * highlight LyneLeft2Default  ctermfg=7 ctermbg=0 guifg=white guibg=#2c2c2c
    autocmd VimEnter,ColorScheme * highlight LyneLeft3Default  ctermfg=8 ctermbg=6 guifg=black guibg=cyan
    " Right Defaults
    autocmd VimEnter,ColorScheme * highlight LyneRight1Default ctermfg=8 ctermbg=4 guifg=black guibg=#16b0f7
    autocmd VimEnter,ColorScheme * highlight LyneRight2Default ctermfg=7 ctermbg=0 guifg=white guibg=#2c2c2c
    " Modified Default
    autocmd VimEnter,ColorScheme * highlight LyneModifiedDefault ctermfg=NONE ctermbg=5 guifg=NONE guibg=#ac54c9
augroup end

" Mode
autocmd VimEnter,ColorScheme * highlight default link LyneModeNormal  LyneModeNormalDefault
autocmd VimEnter,ColorScheme * highlight default link LyneModeInsert  LyneModeInsertDefault
autocmd VimEnter,ColorScheme * highlight default link LyneModeVisual  LyneModeVisualDefault
autocmd VimEnter,ColorScheme * highlight default link LyneModeReplace LyneModeReplaceDefault
" Left
autocmd VimEnter,ColorScheme * highlight default link LyneLeft1 LyneLeft1Default
autocmd VimEnter,ColorScheme * highlight default link LyneLeft2 LyneLeft2Default
" Right
autocmd VimEnter,ColorScheme * highlight default link LyneRight1 LyneRight1Default
autocmd VimEnter,ColorScheme * highlight default link LyneRight2 LyneRight2Default
" Modified
autocmd VimEnter,ColorScheme * highlight default link LyneModified LyneModifiedDefault

