scriptencoding utf-8

let s:lyne_default_currentmode = {
    \ 'n'      : 'N',
    \ 'no'     : 'N·Operator Pending',
    \ 'v'      : 'V',
    \ 'V'      : 'V·Line',
    \ "\<c-v>" : 'V·Block',
    \ 's'      : 'Select',
    \ 'S'      : 'S·Line',
    \ "\<c-s>" : 'S·Block',
    \ 'i'      : 'I',
    \ 'R'      : 'R',
    \ 'Rv'     : 'V·Replace',
    \ 'c'      : 'Command',
    \ 'cv'     : 'Vim Ex',
    \ 'ce'     : 'Ex',
    \ 'r'      : 'Prompt',
    \ 'rm'     : 'More',
    \ 'r?'     : 'Confirm',
    \ '!'      : 'Shell',
    \ 't'      : 'Terminal'
\}

let s:lyne_default_options = {
    \'leftSeparator': '',
    \'leftDivider':   '',
    \'rightSeparator': '',
    \'rightDivider':   '',
    \'helpMode': 1,
    \'modifiedIndicator': '[+]',
    \'noSeparate': ['lyne#getModified'],
    \'modify_color_left_num': -1,
    \'modify_color_right_num': -1
\}

let s:lyne_default_segments = {
    \'left':  [ ['lyne#getMode'], ['lyne#git', 'lyne#getName', 'lyne#getModified'], ['lyne#filetype'] ],
    \'right': [ ['lyne#getColNumber'], [ 'lyne#getLineNumber' ] ]
\}


function! lyne#reload()
    let s:lyne_options = s:lyne_default_options
    let l:user_lyne_options = get(g:, 'lyne_options')
    if (type(l:user_lyne_options) != v:t_dict)
        let l:user_lyne_options = {}
    endif
    call extend(s:lyne_options, l:user_lyne_options)

    if (has('win32') || get(s:lyne_options, 'lyne_no_fancy'))
        let s:lyne_default_options.leftSeparator = '|'
        let s:lyne_default_options.rightSeparator = '|'
        let s:lyne_default_options.leftDivider = '»'
        let s:lyne_default_options.rightDivider = '«'
    endif

    let s:lyne_segments = s:lyne_default_segments
    let l:user_lyne_segments = get(g:, 'lyne_segments')
    if (type(l:user_lyne_segments) != v:t_dict)
        let l:user_lyne_segments = {}
    endif
    call extend(s:lyne_segments, l:user_lyne_segments)

    let s:lyne_currentmode = s:lyne_default_currentmode
    let l:user_lyne_currentmode = get(g:, 'lyne_currentmode')
    if (type(l:user_lyne_currentmode) != v:t_dict)
        let l:user_lyne_currentmode = {}
    endif
    call extend(s:lyne_currentmode, l:user_lyne_currentmode)

    for nr in range(1, winnr('$'))
        call setwinvar(nr, '&statusline', '%!lyne#statusline('.nr.')')
    endfor
endfunction

function! lyne#statusline(winnum)
    let l:bufnum = winbufnr(a:winnum)
    if (l:bufnum < 1)
        call lyne#reload()
        return ''
    endif
    let l:active = a:winnum == winnr()
    let l:modified = getbufvar(l:bufnum, '&modified')
    let l:type = getbufvar(l:bufnum, '&buftype')
    let l:hidden = getbufvar(l:bufnum, '&bufhidden')

    if (l:hidden != '')
        return ''
    endif
    let l:stl = ''

    let l:left_max = len(s:lyne_segments.left)
    let l:i = 1
    for l:segment in s:lyne_segments.left
        if (l:i == s:lyne_options.modify_color_left_num && l:modified)
            " set previous LeftSep background to modified hl group
            execute 'highlight LyneLeftSep'.(l:i-1).' ctermbg='.synIDattr(synIDtrans(hlID('LyneModified')), 'bg', 'cterm').' guibg='.synIDattr(synIDtrans(hlID('LyneModified')), 'bg', 'gui')
            " let next group be modified hl group
            let l:group = 'LyneModified'
        else
            " let current group be normal hl group
            let l:group = 'LyneLeft'.l:i
        endif

        " start new hl group w/ content
        let l:stl .= '%#' . l:group . '#'
        let l:j = 1
        let l:content = ''
        for l:component in l:segment
            if (exists('*' .l:component))
                " only add content if function exists
                if (l:content != '' && l:j != 1 && index(s:lyne_options.noSeparate, l:component) < 0)
                    " if not first element and not in no-separate list, add divider
                    let l:stl .= ' ' . s:lyne_options.leftDivider
                endif
                " get function content
                let l:content = call(l:component, [a:winnum, 'LyneLeft', l:i])
                " add content
                let l:stl .= l:content
            endif
            let l:j += 1
        endfor
        " end new hl group w/ content
        let l:stl .= ' %*'

        " add separator
        if (synIDattr(synIDtrans(hlID(l:group)), 'reverse', 'cterm'))
            let l:fgbg = 'fg'
        else
            let l:fgbg = 'bg'
        endif
        " current hl group background
        let l:ctermbg = synIDattr(synIDtrans(hlID(l:group)), l:fgbg, 'cterm')
        let l:guibg = synIDattr(synIDtrans(hlID(l:group)), l:fgbg, 'gui')
        " next hl group background
        if (l:i == l:left_max)
            if (l:active)
                let l:sep_group = 'StatusLine'
            else
                let l:sep_group = 'StatusLineNc'
            endif
        else
            let l:sep_group = 'LyneLeft' . (l:i + 1)
        endif
        if (synIDattr(synIDtrans(hlID(l:sep_group)), 'reverse', 'cterm'))
            let l:fgbg = 'fg'
        else
            let l:fgbg = 'bg'
        endif
        " next hl group background
        let l:nextCtermbg = synIDattr(synIDtrans(hlID(l:sep_group)), l:fgbg, 'cterm')
        let l:nextGuibg = synIDattr(synIDtrans(hlID(l:sep_group)), l:fgbg, 'gui')
        " highlight current LeftSep
        if (l:ctermbg != '' && l:nextCtermbg != '')
            execute 'highlight LyneLeftSep'.l:i.' ctermfg='.l:ctermbg.' ctermbg='.l:nextCtermbg
        endif
        if (l:guibg != '' && l:nextGuibg != '')
            execute 'highlight LyneLeftSep'.l:i.' guifg='.l:guibg.' guibg='.l:nextGuibg
        endif
        " add LeftSep with hl group
        let l:stl .= '%#LyneLeftSep'.l:i.'#'.s:lyne_options.leftSeparator.'%*'

        let l:i += 1
    endfor

    let l:stl .= '%='

    let l:right_max = len(s:lyne_segments.right)
    let l:i = l:right_max
    for l:segment in s:lyne_segments.right
        if (l:i == s:lyne_options.modify_color_right_num && l:modified)
            " let next group be modified hl group
            let l:group = 'LyneModified'
        else
            " let current group be normal hl group
            let l:group = 'LyneRight'.l:i
        endif

        " add separator
        if (synIDattr(synIDtrans(hlID(l:group)), 'reverse', 'cterm'))
            let l:fgbg = 'fg'
        else
            let l:fgbg = 'bg'
        endif
        " current hl group background
        let l:ctermbg = synIDattr(synIDtrans(hlID(l:group)), l:fgbg, 'cterm')
        let l:guibg = synIDattr(synIDtrans(hlID(l:group)), l:fgbg, 'gui')
        " next hl group background
        if (l:i + 1 == s:lyne_options.modify_color_right_num && l:modified)
            " set previous RightSep background to modified hl group
            let l:sep_group = 'LyneModified'
        else
            if (l:i == l:right_max)
                if (l:active)
                    let l:sep_group = 'StatusLine'
                else
                    let l:sep_group = 'StatusLineNc'
                endif
            else
                let l:sep_group = 'LyneRight' . (l:i + 1)
            endif
        endif
        if (synIDattr(synIDtrans(hlID(l:sep_group)), 'reverse', 'cterm'))
            let l:fgbg = 'fg'
        else
            let l:fgbg = 'bg'
        endif
        " next hl group background
        let l:nextCtermbg = synIDattr(synIDtrans(hlID(l:sep_group)), l:fgbg, 'cterm')
        let l:nextGuibg = synIDattr(synIDtrans(hlID(l:sep_group)), l:fgbg, 'gui')
        " highlight current RightSep
        if (l:ctermbg != '' && l:nextCtermbg != '')
            execute 'highlight LyneRightSep'.l:i.' ctermfg='.l:ctermbg.' ctermbg='.l:nextCtermbg
        endif
        if (l:guibg != '' && l:nextGuibg != '')
            execute 'highlight LyneRightSep'.l:i.' guifg='.l:guibg.' guibg='.l:nextGuibg
        endif
        " add RightSep with hl group
        let l:stl .= '%#LyneRightSep'.l:i.'#'.s:lyne_options.rightSeparator.'%*'

        " start new hl group w/ content
        let l:stl .= '%#' . l:group . '#'
        let l:j = 1
        let l:content = ''
        for l:component in l:segment
            if (exists('*' .l:component))
                " only add content if function exists
                if (l:content != '' && l:j != 1 && index(s:lyne_options.noSeparate, l:component) != 0)
                    " if not first element and not in no-separate list, add divider
                    let l:stl .= ' ' . s:lyne_options.rightDivider
                endif
                " get function content
                let l:content = call(l:component, [a:winnum, 'LyneRight', l:i])
                " add content
                let l:stl .= l:content
            endif
            let l:j += 1
        endfor
        " end new hl group w/ content
        let l:stl .= ' %*'

        let l:i -= 1
    endfor

    return l:stl
endfunction

function lyne#getSep(winnum, i, group, max, left_right)
endfunction

function! lyne#getMode(winnum, groupname, groupnum)
    let l:active = a:winnum == winnr()
    let l:bufnum = winbufnr(a:winnum)
    let l:type = getbufvar(l:bufnum, '&buftype')
    if (!l:active)
        return ''
    endif
    if (l:type ==# 'help')
        return ' Help'
    endif
    let l:mode = mode()

    if (l:mode =~# '\v(v|V|'."\<c-v>".'|s|S|'."\<c-s>".')')
        let l:group = 'LyneModeVisual'
    elseif (l:mode ==# 'i')
        let l:group = 'LyneModeInsert'
    elseif (l:mode =~# '\v(r|R)')
        let l:group = 'LyneModeReplace'
    else
        let l:group = 'LyneModeNormal'
    endif
    execute 'highlight link '.a:groupname.a:groupnum.' '.l:group

    return ' '.s:lyne_currentmode[l:mode]
endfunction

function! lyne#getName(winnum, groupname, groupnum)
    let l:bufnum = winbufnr(a:winnum)
    let l:name = bufname(l:bufnum)
    let l:type = getbufvar(l:bufnum, '&buftype')
    if (l:type ==# 'help')
        let l:name = ' ' . fnamemodify(l:name, ':t:r')
    else
        if (len(l:name) == 0)
            let l:name = ' [No Name]'
        else
            let l:name = ' '.fnamemodify(l:name, ':~:.')
        endif
    endif
    return l:name
endfunction

function! lyne#getModified(winnum, groupname, groupnum)
    let l:modified = getbufvar(winbufnr(a:winnum), '&modified')
    if (l:modified)
        return s:lyne_options.modifiedIndicator
    else
        return ''
    endif
endfunction

function! lyne#getLineNumber(winnum, groupname, groupnum)
    let l:active = a:winnum == winnr()
    if (!l:active)
        return ''
    endif
    return ' ['.(line('.') - 1 ? (line('.') == line('$') ? 'end' : line('.')) : 'beg').']'
endfunction

function! lyne#getSpacer(winnum, groupname, groupnum)
    return ' '
endfunction

function! lyne#getBufferNumber(winnum, groupname, groupnum)
    return ' '.winbufnr(a:winnum)
endfunction

function! lyne#getReadOnly(winnum, groupname, groupnum)
    return ' '
endfunction

function! lyne#filetype(winnum, groupname, groupnum)
    let l:ft = getbufvar(winbufnr(a:winnum), '&filetype')
    if (l:ft ==# 'help')
        return ' '
    endif
    return ' '.l:ft
endfunction

function! lyne#git(winnum, groupname, groupnum)
    let l:active = a:winnum == winnr()
    if (l:active && exists('*fugitive#head'))
        let l:branch = fugitive#head()
        " let l:branch = getbufvar(winbufnr(a:winnum), 'call fugitive#statusline()')
        return l:branch ==# '' ? '' : ' '.l:branch
    endif
    return ''
endfunction

